package code

import (
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	loxe "gitlab.com/loxe-tools/go-loxerror"
	"gitlab.com/loxe-tools/go-loxerror/internal/generate/code/templates"
)

func Helpers(tmpls *templates.Templates, log *logCLI.LogCLI) string {
	helpersCode, e := tmpls.Helpers(templates.HelpersVars{
		templates.LoxerrorTypeName,
		templates.LoxerrorDictionaryTypeName,
		loxe.MsgJsonField,
		loxe.ModuleJsonField,
		loxe.CodeJsonField,
		loxe.DataJsonField,
		loxe.WrappedJsonField,
		loxe.StackJsonField,
		loxe.ModuleNameSeparator,
	})
	if e != nil {
		log.Fatal("Error creating Helpers file: %v", e)
	}

	log.Debug("Ok!")
	return helpersCode
}
