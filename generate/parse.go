package generate

import (
	"flag"
	"gitlab.com/loxe-tools/go-loxerror/internal/generate/input"
)

func parseFlagsCLI(defaultValues input.Arguments) input.Arguments {
	help := "use --help instead"

	flag.StringVar(&defaultValues.OutputFileName, input.MetaᐸArgumentsᐳ.OutputFileName, defaultValues.OutputFileName, help)
	flag.StringVar(&defaultValues.OutputFolderPath, input.MetaᐸArgumentsᐳ.OutputFolderPath, defaultValues.OutputFolderPath, help)
	flag.StringVar((*string)(&defaultValues.OutputLanguage), input.MetaᐸArgumentsᐳ.OutputLanguage, string(defaultValues.OutputLanguage), help)
	flag.BoolVar(&defaultValues.WithHelpers, input.MetaᐸArgumentsᐳ.WithHelpers, defaultValues.WithHelpers, help)

	flag.BoolVar(&defaultValues.Debug, input.MetaᐸArgumentsᐳ.Debug, defaultValues.Debug, help)
	flag.BoolVar(&defaultValues.SupportsANSI, input.MetaᐸArgumentsᐳ.SupportsANSI, defaultValues.SupportsANSI, help)
	flag.StringVar(&defaultValues.Workdir, input.MetaᐸArgumentsᐳ.Workdir, defaultValues.Workdir, help)

	flag.Usage = cliUsage
	flag.Parse()

	return defaultValues
}
