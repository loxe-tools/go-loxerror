package templates

import (
	"bytes"
	loxe "gitlab.com/loxe-tools/go-loxerror"
)

type DictionaryVars struct {
	Module loxe.LoxerrorMod
}

func (t *Templates) Dictionary(vars DictionaryVars) (string, error) {
	var data bytes.Buffer
	e := t.dictionary.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil
}
