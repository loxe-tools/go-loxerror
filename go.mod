module gitlab.com/loxe-tools/go-loxerror

go 1.14

require (
	gitlab.com/loxe-tools/go-base-library v0.0.29
	gitlab.com/loxe-tools/go-validation v1.0.40
)
