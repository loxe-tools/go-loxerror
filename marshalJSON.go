package loxe

import (
	"encoding/json"
	"fmt"
)

func (e *loxerror) MarshalJSON() ([]byte, error) {
	data := "null"
	if e.data != nil {
		jsonData, e_ := json.Marshal(e.data)
		if e_ != nil {
			return nil, e_
		}
		data = string(jsonData)
	}

	wrapped := "null"
	if e.wrapped != nil {
		aux, e := e.wrapped.MarshalJSON()
		if e != nil {
			return nil, e
		}
		wrapped = string(aux)
	}

	marshaledError := fmt.Sprintf(templateMarshalJSON,
		MsgJsonField, e.msg,
		ModuleJsonField, e.Module(),
		CodeJsonField, e.code,
		DataJsonField, data,
		WrappedJsonField, wrapped,
	)
	return []byte(marshaledError), nil
}

const templateMarshalJSON = `{ "%s": "%s", "%s": "%s", "%s": "%d", "%s": %s, "%s": %s }`
const MsgJsonField = "msg"
const ModuleJsonField = "module"
const CodeJsonField = "code"
const DataJsonField = "data"
const WrappedJsonField = "wrapped"
