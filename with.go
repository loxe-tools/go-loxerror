package loxe

func (e *loxerror) With(data interface{}, extraDatas ...interface{}) Loxerror {
	newErr := *e
	newErr.data = data

	if len(extraDatas) != 0 {
		newErr.data = append([]interface{}{data}, extraDatas...)
	}

	return &newErr
}
