package input

import (
	"fmt"
	"os"
	"regexp"
	"strings"
)

func required(s string) error {
	if strIsOnlyWhiteSpace(s) {
		return fmt.Errorf("This flag is required. More info use --help")
	}
	return nil
}

func validLanguage(l OutputLanguage) error {
	if !l.IsValid() {
		return fmt.Errorf("The given language is not recognizable. More info use --help")
	}
	return nil
}

func isEmpty(s string) error {
	if !strIsOnlyWhiteSpace(s) {
		return fmt.Errorf("This cannot be set. More info use --help")
	}
	return nil
}

func validFileName(s string) error {
	if !fileNameRegexp.MatchString(s) {
		return fmt.Errorf("This value is not a valid POSIX file name")
	}
	return nil
}

func validFolderpath(s string) error {
	file, e := os.Open(s)
	if e != nil {
		return fmt.Errorf("Cannot open this path: %v", e)
	}
	defer file.Close()
	stats, e := file.Stat()
	if e != nil {
		return fmt.Errorf("Cannot check this path info: %v", e)
	}
	if !stats.IsDir() {
		return fmt.Errorf("Not a directory")
	}

	return nil
}

// -----

func strIsOnlyWhiteSpace(s string) bool { return strings.TrimSpace(s) == "" }

var fileNameRegexp = func() *regexp.Regexp {
	reg, e := regexp.Compile("^[-_.A-Za-z0-9]+$")
	if e != nil {
		panic(e)
	}
	return reg
}()
