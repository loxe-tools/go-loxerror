package templates

import (
	"bytes"
)

type HelpersVars struct {
	LoxerrorTypeName           string
	LoxerrorDictionaryTypeName string
	MsgFieldName               string
	ModuleFieldName            string
	CodeFieldName              string
	DataFieldName              string
	WrappedFieldName           string
	StackFieldName             string
	ModuleNameSeparator        string
}

func (t *Templates) Helpers(vars HelpersVars) (string, error) {
	var data bytes.Buffer
	e := t.helpers.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil

}
