package code

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	loxe "gitlab.com/loxe-tools/go-loxerror"
	"gitlab.com/loxe-tools/go-loxerror/internal/generate/code/templates"
	"gitlab.com/loxe-tools/go-loxerror/internal/generate/code/templates/typescript"
	"strings"
)

func Dictionary(rootModules []loxe.LoxerrorMod, tmpls *templates.Templates, log *logCLI.LogCLI) string {
	code := ""
	for _, currModule := range rootModules {
		code += typescript.DictionaryHeader(currModule.Name(), templates.LoxerrorDictionaryTypeName) + "\n"
		code += dictionaryFields(currModule, tmpls,
			log.Debug("Generating Dictionary fields code for module '%s'", currModule.Name()), 1)
		code += "\n}\n"
	}

	return code
}

func dictionaryFields(module loxe.LoxerrorMod, tmpls *templates.Templates, log *logCLI.LogCLI, nest uint) string {
	code, e := tmpls.Dictionary(templates.DictionaryVars{module})
	if e != nil {
		log.Fatal("Error: %v", e)
	}
	if nest != 0 {
		code = strings.Repeat(indentationCharacter, int(nest)) + code
		code = strings.ReplaceAll(code, "\n", "\n"+strings.Repeat(indentationCharacter, int(nest)))
	}

	for _, currSubMod := range module.SubModules() {
		subCode := dictionaryFields(currSubMod, tmpls,
			log.Debug("Generating Dictionary fields code for subModule '%s'", currSubMod.Name()), nest+1)

		code += fmt.Sprintf("\n%s// %s\n%s'%s': {\n%s\n%s}",
			strings.Repeat(indentationCharacter, int(nest)),
			currSubMod.FullName(),
			strings.Repeat(indentationCharacter, int(nest)),
			currSubMod.Name(),
			subCode,
			strings.Repeat(indentationCharacter, int(nest)),
		)
	}

	log.Debug("Ok!")
	return code
}

const indentationCharacter = "  "
