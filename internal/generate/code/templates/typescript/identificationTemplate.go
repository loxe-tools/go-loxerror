package typescript

import "gitlab.com/loxe-tools/go-loxerror/internal/generate/code/templates"

const IdentificationTemplate = `{{$LoxerrorTypeName := .LoxerrorTypeName -}}
{{$ModuleFieldName := .ModuleFieldName -}}
{{$CodeFieldName := .CodeFieldName -}}
{{range .Module.DatalessErrors -}}
/**
 * Will test the given {{$LoxerrorTypeName}} to see if it has module
 * equal to '{{.Module}}' and code equal to '{{.Code}}'
 */
function Is{{$LoxerrorTypeName}}_{{` + templates.RemoveModuleNameDotsFuncMapName + ` .Module}}_{{.Code}}(loxerror: {{$LoxerrorTypeName}}): boolean { 
  return loxerror.{{$ModuleFieldName}} === '{{.Module}}' && loxerror.{{$CodeFieldName}} === '{{.Code}}'
}
{{- end}}
{{range .Module.DatafullErrors -}}
/**
 * Will test the given {{$LoxerrorTypeName}} to see if it has module
 * equal to '{{.Module}}' and code equal to '{{.Code}}'
 */
function Is{{$LoxerrorTypeName}}_{{` + templates.RemoveModuleNameDotsFuncMapName + ` .Module}}_{{.Code}}(loxerror: {{$LoxerrorTypeName}}): boolean { 
  return loxerror.{{$ModuleFieldName}} === '{{.Module}}' && loxerror.{{$CodeFieldName}} === '{{.Code}}'
}
{{- end}}`
