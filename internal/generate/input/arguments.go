package input

//go:generate go run gitlab.com/loxe-tools/go-loxerror/internal/generate/input/generate --output-file-name validation --debug --source-type Arguments --source-package gitlab.com/loxe-tools/go-loxerror/internal/generate/input

type Arguments struct {
	OutputFileName   string
	OutputFolderPath string
	OutputLanguage   OutputLanguage
	WithHelpers      bool

	Debug        bool
	SupportsANSI bool
	Workdir      string
}

var DefaultArguments = Arguments{
	"",
	"./",
	"",

	false,
	false,
	true,
	"./",
}
