package generate

import (
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	"gitlab.com/loxe-tools/go-loxerror/internal/generate/code/templates"
	"gitlab.com/loxe-tools/go-loxerror/internal/generate/code/templates/typescript"
	"gitlab.com/loxe-tools/go-loxerror/internal/generate/input"
)

func createLangTemplates(lang input.OutputLanguage, log *logCLI.LogCLI) *templates.Templates {
	switch lang {
	case input.Typescript:
		tmpl, e := templates.NewTemplates(
			typescript.DictionaryTemplate, typescript.HelpersTemplate, typescript.IdentificationTemplate)
		if e != nil {
			log.Fatal("Cannot create Typescript template: %v", e)
		}

		log.Debug("Typescript template created with success")
		return tmpl

	default:
		log.Fatal("The given language is no supported: %s", lang)
		return nil // never executed, since the fatal will exit
	}
}
