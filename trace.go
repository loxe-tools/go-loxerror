package loxe

const minimumStackDepth = uint(8)
const defaultStackDepth = uint(32)
const maximumStackDepth = uint(128)

const minimumFrameSkip = uint(3)

// Trace has two optional uint parameters: stack (index 0) and
// depth (index  1).
//
// Example:
//  error.Trace(skip, depth)
//
// Note that you can pass:
// - Nothing
// - Just the skip
// - Skip and depth
//
// If the depth parameter is lesser/greater than the
// minimum/maximum bounds, the limit will be used
//
// If the skip is greater/equal to the depth, the skip will
// be set to "depth - 1". If the skip is less than 3 (minimum),
// it will be set to 3
func (e *loxerror) Trace(stackConfig ...uint) Loxerror {
	skip := minimumFrameSkip
	if len(stackConfig) >= 1 {
		skip += stackConfig[0]
	}

	depth := defaultStackDepth
	if len(stackConfig) >= 2 {
		depth = stackConfig[1]
	}
	if depth < minimumStackDepth {
		depth = minimumStackDepth
	}
	if depth > maximumStackDepth {
		depth = maximumStackDepth
	}

	if skip >= depth {
		skip = depth - 1
	}

	newErr := *e
	newErr.stack = newStack(depth, skip)
	return &newErr
}
