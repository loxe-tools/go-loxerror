package templates

import (
	"strings"
	"text/template"
)

const RemoveModuleNameDotsFuncMapName = "removeDots"

func NewTemplates(dictionaryTmpl, helpersTmpl, identificationTmpl string) (*Templates, error) {
	dict, e := template.New("").Parse(dictionaryTmpl)
	if e != nil {
		return nil, e
	}
	helpers, e := template.New("").Parse(helpersTmpl)
	if e != nil {
		return nil, e
	}
	funcMaps := template.FuncMap{
		RemoveModuleNameDotsFuncMapName: func(s string) string { return strings.ReplaceAll(s, ".", "") }}
	id, e := template.New("").Funcs(funcMaps).Parse(identificationTmpl)
	if e != nil {
		return nil, e
	}

	return &Templates{
		dictionary:     dict,
		helpers:        helpers,
		identification: id,
	}, nil
}

type Templates struct {
	dictionary     *template.Template
	helpers        *template.Template
	identification *template.Template
}
