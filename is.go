package loxe

// Is check to see if a standard error interface has
// a specific Loxerror as the underlying concrete value.
//
// Note that the module and code of the given Loxerror must
// match the ones of the method receiver
func (e *loxerror) Is(otherLoxerror Loxerror) bool {
	return e.Code() == otherLoxerror.Code() && e.Module() == otherLoxerror.Module()
}
