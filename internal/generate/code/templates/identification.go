package templates

import (
	"bytes"
	loxe "gitlab.com/loxe-tools/go-loxerror"
)

type IdentificationVars struct {
	Module           loxe.LoxerrorMod
	LoxerrorTypeName string
	ModuleFieldName  string
	CodeFieldName    string
}

func (t *Templates) Identification(vars IdentificationVars) (string, error) {
	var data bytes.Buffer
	e := t.identification.Execute(&data, vars)
	if e != nil {
		return "", e
	}

	return data.String(), nil

}
