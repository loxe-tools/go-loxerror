package generate

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/brand"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	loxe "gitlab.com/loxe-tools/go-loxerror"
	"gitlab.com/loxe-tools/go-loxerror/internal"
	"gitlab.com/loxe-tools/go-loxerror/internal/generate"
	"gitlab.com/loxe-tools/go-loxerror/internal/generate/input"
)

type Arguments = input.Arguments

const Typescript = input.Typescript

func Generate(defaultArguments input.Arguments, rootModules ...loxe.LoxerrorMod) {
	brand.ToStdout(fmt.Sprintf("%s %s", cliTitle, internal.LibraryModuleVersion))

	arguments := parseFlagsCLI(defaultArguments)
	log := logCLI.NewLogCLI(arguments.Debug, arguments.SupportsANSI)

	generate.Generate(arguments, log, rootModules...)
}

const cliTitle = "Löxerror - Client code generator"
