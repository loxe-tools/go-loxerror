package typescript

const HelpersTemplate = `/**
 * High-level representation of a {{.LoxerrorTypeName}} module dictionary
*/
type {{.LoxerrorDictionaryTypeName}} = {
  [key in string]: string | ((data: unknown) => string) | {{.LoxerrorDictionaryTypeName}}
}

/**
 * This function is used to translate the message of some
 * {{.LoxerrorTypeName}}
 *
 * The {{.LoxerrorTypeName}} code will be used to search the translated 
 * error message inside the dictionary
 *
 * Note that if the {{.LoxerrorTypeName}} has some data that needs to be
 * displayed in the error message, this function will handle
 * it
 *
 * @param loxerror
 *      The {{.LoxerrorTypeName}} that contains the message to be translated
 * @param dictionary
 *      The dictionary to search for the translated message
 * @param fallback
 *      If the {{.LoxerrorTypeName}} is not found inside the dictionary, use it
 */
function Translate{{.LoxerrorTypeName}}(loxerror: {{.LoxerrorTypeName}}, dictionary: {{.LoxerrorDictionaryTypeName}}, fallback: string): string {
  let shouldReturnFallback = false
  let module = dictionary
  loxerror.{{.ModuleFieldName}}.split('{{.ModuleNameSeparator}}').forEach(moduleName => {
    const currSubModule = module[moduleName]
    if (currSubModule === null || currSubModule === undefined || typeof currSubModule !== 'object') {
      shouldReturnFallback = true
      return
    }
    module = currSubModule
  })
  if (shouldReturnFallback) {
    return fallback
  }

  const msg = module[loxerror.{{.CodeFieldName}}]
  if (typeof msg === 'string') {
    return msg
  }
  if (typeof msg === 'function') {
    return msg(loxerror.{{.DataFieldName}})
  }
  return fallback
}

/**
 * Representation of a {{.LoxerrorTypeName}}
 *
 * If the {{.LoxerrorTypeName}} has any data, the first generic type parameter
 * is required
 *
 * If the {{.LoxerrorTypeName}} is wrapping another one, the second type parameter
 * is required.
 *
 * Note that the codes are encoded as strings just to avoid issues with
 * BigInt
 */
export interface {{.LoxerrorTypeName}}<T = unknown, U extends {{.LoxerrorTypeName}} | null = null> {
  {{.MsgFieldName}}: string
  {{.ModuleFieldName}}: string
  {{.CodeFieldName}}: string
  {{.DataFieldName}}?: T
  {{.WrappedFieldName}}?: U
  {{.StackFieldName}}?: string
}

/**
 * This function will take some parameter and check to
 * see if it is a {{.LoxerrorTypeName}}
 *
 * Note that the msg, module and code fields are required
 * to be not empty
 *
 * The data, wrapped and stack fields can be null/empty or
 * nonexistent
 *
 * The wrapped field, if present, will be used as an
 * argument to recursively call this function. If it is not
 * a {{.LoxerrorTypeName}}, the entire call will return false
 *
 * If the given parameter has any field different than the
 * ones listed above, it is not considered a {{.LoxerrorTypeName}}
 *
 * @param obj
 *      This parameter will be checked to see if it is a
 *      {{.LoxerrorTypeName}}
 */
export function Is{{.LoxerrorTypeName}}(obj: any): obj is {{.LoxerrorTypeName}} {
  if (typeof obj !== 'object') {
    return false
  }
  
  const keysLength = Object.keys(obj).length
  if (keysLength < 2 || keysLength > 6) {
    return false
  }
  
  const has_{{.MsgFieldName}} = !!obj.{{.MsgFieldName}} && (typeof obj.{{.MsgFieldName}} === 'string')
  const has_{{.ModuleFieldName}} = obj.hasOwnProperty('{{.ModuleFieldName}}') && (typeof obj.{{.ModuleFieldName}} === 'string')
  const has_{{.CodeFieldName}} = !!obj.{{.CodeFieldName}} && (typeof obj.{{.CodeFieldName}} === 'string')
  if (!has_{{.MsgFieldName}} || !has_{{.ModuleFieldName}} || !has_{{.CodeFieldName}}) {
    return false
  }
  if (keysLength === 3) {
      return true
  }

  const has_{{.DataFieldName}} = obj.hasOwnProperty('{{.DataFieldName}}')
  const has_{{.WrappedFieldName}} = obj.hasOwnProperty('{{.WrappedFieldName}}') && IsLoxerror(obj.{{.WrappedFieldName}})
  const has_{{.StackFieldName}} = obj.hasOwnProperty('{{.StackFieldName}}') && (typeof obj.{{.StackFieldName}} === 'string')

  let sum = has_{{.DataFieldName}} ? 1 : 0
  sum += has_{{.StackFieldName}} ? 1 : 0
  sum += has_{{.WrappedFieldName}} ? 1 : 0

  return sum === (keysLength - 3)
}
`
