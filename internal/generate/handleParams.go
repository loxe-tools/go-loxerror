package generate

import (
	"gitlab.com/loxe-tools/go-base-library/logCLI"
	loxe "gitlab.com/loxe-tools/go-loxerror"
	"gitlab.com/loxe-tools/go-loxerror/internal/generate/input"
)

func handleParams(arguments input.Arguments, log *logCLI.LogCLI, rootModules []loxe.LoxerrorMod) input.Arguments {
	validateLog := log.Debug("Validating arguments...")
	input.Validate(arguments, validateLog)
	validateRootModules(rootModules, validateLog)

	normalizeLog := log.Debug("Normalizing arguments...")
	arguments = input.Normalize(arguments, normalizeLog)
	input.PrintArguments(arguments, normalizeLog)

	return arguments
}

func validateRootModules(rootModules []loxe.LoxerrorMod, log *logCLI.LogCLI) {
	if len(rootModules) == 0 {
		log.Fatal("You cannot generate code without passing the modules")
	}

	for i, currModuleI := range rootModules {
		for j := i + 1; j < len(rootModules); j++ {
			currModuleJ := rootModules[j]
			if currModuleI.Name() == currModuleJ.Name() {
				log.Fatal("You cannot pass two root modules with the same name: '%s'", currModuleJ.Name())
				return
			}
		}
	}
}
