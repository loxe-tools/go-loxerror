package typescript

func DictionaryHeader(moduleName, loxerrorTypeName string) string {
	return `/**
 * ` + loxerrorTypeName + ` dictionary
 * Use it to translate the error messages
 *
 * Note that the codes are encoded as strings
 * just to avoid issues with BigInt
*/
export type ` + loxerrorTypeName + `_` + moduleName + ` = {`
}

const DictionaryTemplate = `{{ range .Module.DatalessErrors -}}
// {{.Error}}
'{{.Code}}': string
{{ end -}}
{{ range .Module.DatafullErrors -}}
// {{.Error}}
'{{.Code}}': (data: unknown) => string
{{- end }}`
