package input

import (
	"fmt"
	"gitlab.com/loxe-tools/go-base-library/logCLI"
)

func PrintArguments(args Arguments, log *logCLI.LogCLI) {
	log.Debug(fmt.Sprintf(argsTmpl,
		MetaᐸArgumentsᐳ.OutputFileName, args.OutputFileName,
		MetaᐸArgumentsᐳ.OutputFolderPath, args.OutputFolderPath,
		MetaᐸArgumentsᐳ.OutputLanguage, args.OutputLanguage,
		MetaᐸArgumentsᐳ.WithHelpers, args.WithHelpers,

		MetaᐸArgumentsᐳ.Debug, args.Debug,
		MetaᐸArgumentsᐳ.SupportsANSI, args.SupportsANSI,
		MetaᐸArgumentsᐳ.Workdir, args.Workdir,
	))
}

const argsTmpl = `CLI arguments:
Output arguments:
	--%s: %s
	--%s: %s
	--%s: %s
	--%s: %t
Other arguments:
	--%s: %t
	--%s: %t
	--%s: %s`
