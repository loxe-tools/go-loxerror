package loxe

import "fmt"

type LoxerrorMod = *loxerrorMod

func LoxerrorModule(name string) LoxerrorMod {
	return &loxerrorMod{
		nil,
		name,
		map[uint64]*loxerror{},
		map[uint64]*loxerror{},
		map[string]*loxerrorMod{},
	}
}

//------

type loxerrorMod struct {
	parent        *loxerrorMod
	name          string
	datalessError map[uint64]*loxerror
	datafullError map[uint64]*loxerror
	subModules    map[string]*loxerrorMod
}

func (m *loxerrorMod) DataLoxerror(code uint64, msg string, formatter func(msg string, args ...interface{}) string) DatafullLoxerror {
	_, existsDataless := m.datalessError[code]
	_, existsDatafull := m.datafullError[code]
	if existsDataless || existsDatafull {
		panic(fmt.Errorf("the given error code is already in use: %d", code))
	}

	newLoxerror := &loxerror{
		msg,
		m,
		code,
		nil,
		nil,
		nil,
	}
	m.datafullError[code] = newLoxerror
	return func(data interface{}, extraDatas ...interface{}) Loxerror {
		newLoxerror := newLoxerror.
			Trace(1).
			With(data, extraDatas...)
		newLoxerror.msg = formatter(msg, append([]interface{}{data}, extraDatas...)...)
		return newLoxerror
	}
}

func (m *loxerrorMod) Loxerror(code uint64, msg string) DatalessLoxerror {
	_, existsDataless := m.datalessError[code]
	_, existsDatafull := m.datafullError[code]
	if existsDataless || existsDatafull {
		panic(fmt.Errorf("the given error code is already in use: %d", code))
	}

	newLoxerror := &loxerror{
		msg,
		m,
		code,
		nil,
		nil,
		nil,
	}
	m.datalessError[code] = newLoxerror
	return func() Loxerror { return newLoxerror.Trace(1) }
}

func (m *loxerrorMod) SubModule(name string) LoxerrorMod {
	_, exists := m.subModules[name]
	if exists {
		panic(fmt.Errorf("the given module name is already in use: %s", name))
	}

	newModule := &loxerrorMod{
		m,
		name,
		map[uint64]*loxerror{},
		map[uint64]*loxerror{},
		map[string]*loxerrorMod{},
	}
	m.subModules[name] = newModule
	return newModule
}

func (m *loxerrorMod) FullName() string {
	if m.parent == nil {
		return m.name
	}

	return m.parent.FullName() + ModuleNameSeparator + m.name
}

func (m *loxerrorMod) Name() string                        { return m.name }
func (m *loxerrorMod) DatalessErrors() map[uint64]Loxerror { return m.datalessError }
func (m *loxerrorMod) DatafullErrors() map[uint64]Loxerror { return m.datafullError }
func (m *loxerrorMod) SubModules() map[string]LoxerrorMod  { return m.subModules }

//-----

const ModuleNameSeparator = "."

type DatafullLoxerror func(data interface{}, extraDatas ...interface{}) Loxerror
type DatalessLoxerror func() Loxerror

func (d DatafullLoxerror) Is(otherLoxerror Loxerror) bool { return d(nil).Is(otherLoxerror) }
func (d DatalessLoxerror) Is(otherLoxerror Loxerror) bool { return d().Is(otherLoxerror) }
