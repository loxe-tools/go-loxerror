package loxe

type Loxerror = *loxerror

//-----

type loxerror struct {
	msg     string
	module  *loxerrorMod
	code    uint64
	data    interface{}
	wrapped *loxerror
	stack   *stack
}

func (e *loxerror) Error() string     { return e.msg }
func (e *loxerror) Module() string    { return e.module.FullName() }
func (e *loxerror) Code() uint64      { return e.code }
func (e *loxerror) Data() interface{} { return e.data }
func (e *loxerror) Unwrap() Loxerror  { return e.wrapped }
