package input

var schemaArguments = NewSchemaᐸArgumentsᐳ().
	Fields(func(schema SchemaFieldsᐸArgumentsᐳ) SchemaFieldsᐸArgumentsᐳ {
		return schema.
			OutputFileName(outputFileNameRule).
			OutputFolderPath(outputFolderPathRule).
			OutputLanguage(outputLanguageRule).
			Workdir(workdirRule)
	})

// -----

var outputFileNameRule = Intersectionᐸstringᐳ(required, validFileName)
var outputFolderPathRule = Unionᐸstringᐳ(isEmpty, validFolderpath)
var outputLanguageRule = IntersectionᐸOutputLanguageᐳ(func(l OutputLanguage) error { return required(string(l)) }, validLanguage)

var workdirRule = Unionᐸstringᐳ(isEmpty, validFolderpath)
