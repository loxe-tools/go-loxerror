package loxe

import (
	"fmt"
	"runtime"
)

func newStack(depth, skip uint) *stack {
	pcs := make([]uintptr, depth)
	n := runtime.Callers(int(skip), pcs)
	st := stack(pcs[0:n])
	return &st
}

type stack []uintptr

func (s *stack) toJson() string     { return s.toString(framePrintToJsonTmpl) }
func (s *stack) toTerminal() string { return s.toString(framePrintToTerminalTmpl) }

func (s *stack) toString(template string) string {
	str := ""
	frames := runtime.CallersFrames(*s)
	for {
		frame, hasNext := frames.Next()
		str += fmt.Sprintf(template, frame.Func.Name(), frame.File, frame.Line)

		if !hasNext {
			return str
		}
	}
}

const framePrintToJsonTmpl = `%s\n\t%s:%d\n`

const framePrintToTerminalTmpl = `	%s
		%s:%d
`
