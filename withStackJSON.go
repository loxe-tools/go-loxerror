package loxe

import (
	"encoding/json"
	"fmt"
)

func (e *loxerror) WithStackJSON() ([]byte, error) {
	stack := ""
	if e.stack != nil {
		stack = e.stack.toJson()
	}

	data := "null"
	if e.data != nil {
		jsonData, e_ := json.Marshal(e.data)
		if e_ != nil {
			return nil, e_
		}
		data = string(jsonData)
	}

	wrapped := "null"
	if e.wrapped != nil {
		aux, e := e.wrapped.WithStackJSON()
		if e != nil {
			return nil, e
		}
		wrapped = string(aux)
	}

	marshaledError := fmt.Sprintf(templateNoStackJSON,
		MsgJsonField, e.msg,
		ModuleJsonField, e.Module(),
		CodeJsonField, e.code,
		DataJsonField, data,
		WrappedJsonField, wrapped,
		StackJsonField, stack,
	)
	return []byte(marshaledError), nil
}

const templateNoStackJSON = `{ "%s": "%s", "%s": "%s", "%s": "%d", "%s": %s, "%s": %s, "%s": "%s" }`
const StackJsonField = "stack"
